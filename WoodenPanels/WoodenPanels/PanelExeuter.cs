﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Newtonsoft.Json;
using System.IO;
using WoodenPanels.Data;


namespace WoodenPanels
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class PanelExeuter : IExternalCommand
    {
        static string _family_folder = Path.GetDirectoryName(typeof(PanelExeuter)
        .Assembly.Location);
        const string _family_ext = "json";
        static string _family_path = null;
        static string FamilyPath
        {
            get
            {
                if (null == _family_path)
                {
                    _family_path = Path.Combine(
                      _family_folder, "options");

                    _family_path = Path.ChangeExtension(
                      _family_path, _family_ext);
                }
                return _family_path;
            }
        }
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication revitApp = commandData.Application;
            UIDocument uidoc = revitApp.ActiveUIDocument;
            Document doc = uidoc.Document;
            UIData uiData = JsonConvert.DeserializeObject<UIData>(File.ReadAllText(FamilyPath));
            
            PanelCreator.CreatePanel(doc, uidoc, uiData,revitApp.Application);
            return Result.Succeeded;
        }
    }
}
