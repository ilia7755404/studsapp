﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using WoodenPanels.Utils;
using System.Reflection;
using MathNet.Spatial.Euclidean;
using System.IO;
using Newtonsoft.Json;


namespace WoodenPanels.PanelCreater
{
    internal static class StudCreator
    {
        
        private static string FamilyName = "Stud";
        static string _family_folder = Path.GetDirectoryName(typeof(StudCreator)
        .Assembly.Location);
        const string _family_ext = "rfa";
        static string _family_path = null;

        public const double _offset = 0.125;
        public const double _startOffset = 0.072916667;
		public const double _commonStep = 10.75;
		public const double _firstStep = 9.5;
		static string FamilyPath
        {
            get
            {
                if (null == _family_path)
                {
                    _family_path = Path.Combine(
                      _family_folder, FamilyName);

                    _family_path = Path.ChangeExtension(
                      _family_path, _family_ext);
                }
                return _family_path;
            }
        }
        internal static void CreateStud(Document doc, 
			IList<Wall> walls, 
			string type_Name,
			string firstStudOffset, 
			View view,
			UIDocument uidoc)
        {
            GetFamily(doc);
            //PlaceHorizonatlStuds(doc, faces, type_Name);
            PlaceVerticalStuds(doc, type_Name, walls,firstStudOffset, view, uidoc);
        }
        private static void PlaceVerticalStuds(Document doc,
			string type_Name, 
			IList<Wall> walls,
			string firstStudOffset,
			View sectView,
			UIDocument uidoc)
        {
			double inches = UnitConvert.InchToFeet(Convert.ToDouble(firstStudOffset));
			Family family = FindElementByName(doc) as Family;
			FamilySymbol familySymbol;

			familySymbol = family.GetFamilySymbolIds()
				.Where(p => (family.Document.GetElement(p) as FamilySymbol).Name == type_Name)
				.Select(p => family.Document.GetElement(p) as FamilySymbol).First();


			Dictionary<Reference, List<Reference>> allignmentsItems = new Dictionary<Reference, List<Reference>>();
			Dictionary<int, List<int>> allignmentsItemsTest = new Dictionary<int, List<int>>();
			Dictionary<ElementId, List<ElementId>> allignmentsItemsElements = new Dictionary<ElementId, List<ElementId>>();

			List<ElementId> studs = new List<ElementId>();

			double upConstrainValue = 0.0;
			double downConstrainValue = 0.0;

			foreach (Wall hostWall in walls)
			{

				DivideCurtaiWall(hostWall, doc);

				using (Transaction tx = new Transaction(doc))
				{
					tx.Start("Creation studs");

					{
						//var viewType3d = new FilteredElementCollector(doc).OfClass(typeof(ViewFamilyType)).OfType<ViewFamilyType>()
						//.FirstOrDefault(v => v.ViewFamily == ViewFamily.ThreeDimensional);

						//var view3D = (viewType3d != null) ? View3D.CreateIsometric(doc, viewType3d.Id) : null;
						//var ref_Mat1 = new FilteredElementCollector(doc).OfClass(typeof(Material))
						//.FirstOrDefault(m => m.Name == "ref_1");

						//var ref_Mat2 = new FilteredElementCollector(doc).OfClass(typeof(Material))
						//.FirstOrDefault(m => m.Name == "ref_2");


						if (!familySymbol.IsActive)
						{ familySymbol.Activate(); doc.Regenerate(); }
						




						Curve locCurve = (hostWall.Location as LocationCurve).Curve;
						var startPoint = locCurve.GetEndPoint(0);
						var endPoint = locCurve.GetEndPoint(1);
						double wallHeight = hostWall.get_Parameter(BuiltInParameter.WALL_USER_HEIGHT_PARAM).AsDouble();


						Reference mlnTopRef = null;
						Reference mlnBotRef = null;


						List<Reference> studTopFaces = new List<Reference>();
						List<Reference> studBotFaces = new List<Reference>();


						List<Mullion> mullions = hostWall.CurtainGrid.GetMullionIds()
						.Select(id => doc.GetElement(id) as Mullion)
						.ToList();

						var topMlnValue = mullions.Max(mln => mln.LocationCurve.GetEndPoint(0).Z);
						var botMlnValue = mullions.Min(mln => mln.LocationCurve.GetEndPoint(0).Z);

						var topMln = mullions.FirstOrDefault(ml => ml.LocationCurve.GetEndPoint(0).Z == topMlnValue);
						var botMln = mullions.FirstOrDefault(ml => ml.LocationCurve.GetEndPoint(0).Z == botMlnValue);

						mlnTopRef = GetStudReference(topMln as FamilyInstance, sectView, -1, doc);
						mlnBotRef = GetStudReference(botMln as FamilyInstance, sectView, -1, doc);

						Point2D WallVectorStart = new Point2D(startPoint.X, startPoint.Y);
						Point2D WallVectorEnd = new Point2D(endPoint.X, endPoint.Y);
						Vector2D WallVector = WallVectorEnd - WallVectorStart;
						Vector2D WallNormalVector = (WallVectorEnd - WallVectorStart).Normalize();
						List<double> listRange = new List<double>();
						double lengthWall = WallVector.Length;

						listRange.Add(inches);
						if (inches - 0.072916667 < 1e-5)
						{
							listRange.Add(UnitConvert.InchToFeet(Convert.ToDouble(_firstStep)) + inches);
						}
						var step_2 = UnitConvert.InchToFeet(Convert.ToDouble(_commonStep));

						while (listRange.Last() + step_2 < lengthWall)
						{
							var last = listRange.Last();
							listRange.Add(step_2 + last);
						}
						var lastStud = lengthWall - inches;
						var preLast = listRange.Last();
						var prePreLast = listRange[listRange.Count - 2];

						if (preLast + step_2 - lastStud > 1e-5)
						{
							listRange.Add((lastStud - prePreLast) / 2 + prePreLast);
							listRange.Remove(preLast);
						}
						listRange.Add(lastStud);

						//Reference faceRef = hostWall.GetFaceForFamily(doc);
						Face face = hostWall.GetPanelFaceForFamily(doc);
						Reference faceRef = face.Reference;
						
						//var faceVertices = face.Triangulate().Vertices.ToList();
						var faceVertices = hostWall.GetTransformPoints(face, doc).ToList();
						listRange.ForEach(r =>
						{
							Vector2D roundNormalVector = new Vector2D(WallNormalVector.X, WallNormalVector.Y);
							Point2D newPoint = new Point2D(startPoint.X, startPoint.Y) + roundNormalVector * r;
							XYZ ipStart = Project(faceVertices, new XYZ(newPoint.X, newPoint.Y, startPoint.Z + _offset));
							XYZ ipEnd = Project(faceVertices, new XYZ(ipStart.X, ipStart.Y, ipStart.Z + wallHeight - _offset * 2));
							Line line = Line.CreateBound(ipStart, ipEnd);

							FamilyInstance stud = doc.Create.NewFamilyInstance(faceRef, line, familySymbol);
							doc.Regenerate();

							studs.Add(stud.Id);
							upConstrainValue = ipEnd.Z;
							downConstrainValue = ipStart.Z;

							//studTopFaces.Add(GetStudReference(stud, sectView, 1, doc));
							//studBotFaces.Add(GetStudReference(stud, sectView, -1, doc));
							studTopFaces.Add(stud.GetReferenceByName("Right"));
							studBotFaces.Add(stud.GetReferenceByName("Left"));

						});
						allignmentsItems.Add(mlnTopRef, studTopFaces);
						allignmentsItems.Add(mlnBotRef, studBotFaces);

					}
					tx.Commit();
				}
			}

			using (Transaction tx = new Transaction(doc))
            {
                tx.Start("Creation alignment");
				try
				{
					foreach (var item in allignmentsItems)
					{
						Reference mlnFaceRef = item.Key;
						foreach (Reference ref_twoId in item.Value)
						{
							doc.Create.NewAlignment(sectView, mlnFaceRef, ref_twoId);
						}
					}
				}
				catch { }
				foreach (var item in allignmentsItemsElements)
				{

				}
				doc.Delete(sectView.Id);
				tx.Commit();
            }
        }

		internal static void DivideCurtaiWall(Wall hostWall, Document doc) {
			//Curtain wall dividing
			try
			{
				XYZ firstPoint = (hostWall.Location as LocationCurve).Curve.GetEndPoint(0);
				List<double> heightRange = new List<double>();
				double panelH = 4;
				double cwH = hostWall.get_Parameter(BuiltInParameter.WALL_USER_HEIGHT_PARAM).AsDouble();
				int numberIntPanels = (int)(cwH / panelH);
				for (int i = 0; i < numberIntPanels - 1; i++)
				{
					if (i == 0)
					{
						heightRange.Add(panelH);
					}
					else
					{
						heightRange.Add(heightRange.Last() + panelH);
					}
				}
				double hlast = heightRange.Last();
				double rest = cwH - hlast;
				if (rest > panelH + 1)
				{
					heightRange.Add(hlast + panelH);
				}
				else
				{
					heightRange.Add(hlast + panelH / 2);
				}
				using (Transaction tr = new Transaction(doc))
				{
					tr.Start("Curtain wall dividing");
					foreach (var height in heightRange)
					{
						if (height < cwH)
						{
							hostWall.CurtainGrid.AddGridLine(true, new XYZ(firstPoint.X, firstPoint.Y, firstPoint.Z + height), true);
						}

					}
					tr.Commit();
				}
			}
			catch
			{
			}
		}
		static private Reference FindMullionReference(Mullion mln, bool isTop, View view) {

			var solidGeomOpt = new Autodesk.Revit.DB.Options();
			solidGeomOpt.ComputeReferences = true;
			solidGeomOpt.View = view;

			var geomElem = mln.get_Geometry(solidGeomOpt);
			Reference refFace = null;
			double refValue;
			foreach (GeometryInstance geom in geomElem)
			{
				var geomInstance = geom.GetInstanceGeometry();
				if (geomInstance != null)
				{
					foreach (var geomObj in geomInstance)
					{

						try {
							var solid = geomObj as Solid;
							if (solid != null && solid.Faces.IsEmpty!=true)
							{
								var faces = solid.Faces;
								List<PlanarFace> fList = new List<PlanarFace>();
								var vertices = new List<XYZ>();
								foreach (Face face in faces) {
									vertices.AddRange(face.Triangulate().Vertices);
									fList.Add(face as PlanarFace);
								}
								vertices.Distinct();

								if (isTop == false)
								{
									refValue = vertices
									.Select(m => m.Z)
									.Min();
								}
								else
								{
									refValue = vertices
									.Select(m => m.Z)
									.Max();
								}

								var faceR = fList.FirstOrDefault(face => face.FaceNormal.Z == -1);
								return faceR.Reference;
							}
						} catch {
						}
					}
				}
			}
			if (refFace != null) {
				return refFace;
			}
			return null;
		}
		static private Solid ExtractFirstSolidByFamilyInstance(FamilyInstance instance) {

			var options = new Autodesk.Revit.DB.Options();
			options.ComputeReferences = true;
			options.DetailLevel = ViewDetailLevel.Undefined;
			options.IncludeNonVisibleObjects = true;

			var geomElem = instance.get_Geometry(options);

			foreach (GeometryInstance geom in geomElem) {

				var geomInst = geom.GetInstanceGeometry();
				
				foreach (var geomObj in geomInst)
				{
					if (geomObj is Solid)
					{
						Solid solid = geomObj as Solid;
						if (solid.Faces.IsEmpty == false)
						{
							return solid;
						}
					}

				}
				
			}
			return null;
		}
		static private Face FindFaceByValue(FaceArray faces, double refValue)
		{
			for (int i = 0; i < faces.Size; i++)
			{
				var mlnFace = faces.get_Item(i);
				var trng = mlnFace.Triangulate();
				for (int n = 0; n < trng.NumTriangles; n++)
				{
					var triangle = trng.get_Triangle(n);
					
					XYZ pt0 = triangle.get_Vertex(0);
					XYZ pt1 = triangle.get_Vertex(1);
					XYZ pt2 = triangle.get_Vertex(2);
					if (pt0.Z == refValue & pt1.Z == refValue & pt2.Z == refValue)
					{
						return mlnFace;
					}
				}
			}


			return null;
		}
		static private XYZ Project(List<XYZ> xyzArray, Autodesk.Revit.DB.XYZ point)
        {
            Autodesk.Revit.DB.XYZ a = xyzArray[0] - xyzArray[1];
            Autodesk.Revit.DB.XYZ b = xyzArray[0] - xyzArray[2];
            Autodesk.Revit.DB.XYZ c = point - xyzArray[0];

            Autodesk.Revit.DB.XYZ normal = (a.CrossProduct(b));

            try
            {
                normal = normal.Normalize();
            }
            catch (Exception)
            {
                normal = Autodesk.Revit.DB.XYZ.Zero;
            }

            Autodesk.Revit.DB.XYZ retProjectedPoint = point - (normal.DotProduct(c)) * normal;
            return retProjectedPoint;
        }
        private static Element FindElementByName(Document doc)
	    {
            return new FilteredElementCollector(doc)
              .OfClass(typeof(Family))
              .FirstOrDefault<Element>(
                e => e.Name.Equals(FamilyName));
        }
        private static void GetFamily (Document doc)
        {
            Family family = FindElementByName(doc) as Family;
            
            if (null == FindElementByName(doc))
            {
                using (Transaction tx = new Transaction(doc))
                {
                    tx.Start("Load Family");
                    doc.LoadFamily(FamilyPath, out family);
                    tx.Commit();
                }
            }

        }
		private static Reference GetStudReference(
			FamilyInstance inst,
			View sectView, 
			int zDirection,
			Document doc)
		{
			Reference indexRef = null;
			Autodesk.Revit.DB.Options geomOptions = doc.Application.Create.NewGeometryOptions();
			if (geomOptions != null)
			{
				geomOptions.ComputeReferences = true;
				geomOptions.IncludeNonVisibleObjects = true;
				geomOptions.View = sectView;
			}
			GeometryElement gElement = inst.get_Geometry(geomOptions);
			GeometryInstance gInst = gElement.First() as GeometryInstance;

			if (gInst != null)
			{
				GeometryElement gSymbol = gInst.GetSymbolGeometry();
				if (gSymbol != null && gSymbol.Count() > 0)
				{
					Solid solid = null;
					foreach (var sl in gSymbol) { 
					if(sl is Solid){
							var sll = sl as Solid;
							if (sll.Faces.IsEmpty == false) {
								solid = sll;
							}
						}
					}
					List<PlanarFace> listFaces = new List<PlanarFace>();
					foreach (var f in solid.Faces) {
						listFaces.Add(f as PlanarFace);
					}
					//Face face = FindFaceByValue(solid.Faces, upConstrValue);
					Face face = listFaces.FirstOrDefault(fc => fc.FaceNormal.X == zDirection);
					if (face != null) {
						return GetRefFromFace(doc, inst, face);
					}
				}
			}
			return indexRef;
		}

		private static Reference GetRefFromFace(Document doc, FamilyInstance inst, Face face) {
			Reference indexRef = null;
			String sampleStableRef = null;
			String customStableRef = null;
			sampleStableRef = face.Reference.ConvertToStableRepresentation(doc);
			if (sampleStableRef != null)
			{
				String[] tokenList = sampleStableRef.Split(new char[] { ':' });
				customStableRef = tokenList[0] + ":" + tokenList[1] + ":" + tokenList[2] + ":" + tokenList[3] + ":" + tokenList[4];
				indexRef = Reference.ParseFromStableRepresentation(doc, customStableRef);
				GeometryObject geoObj = inst.GetGeometryObjectFromReference(indexRef);
				if (geoObj != null)
				{
					String finalToken = "";
					if (geoObj is Face)
					{
						finalToken = ":SURFACE";
					}
					customStableRef += finalToken;
					indexRef = Reference.ParseFromStableRepresentation(doc, customStableRef);
					return indexRef;
				}

			}
			return indexRef;
		}
		private static void Test(
			UIDocument uidoc,
			View sectionView)
		{
			Document doc = uidoc.Document;
			Reference reference1 = uidoc.Selection.PickObject(ObjectType.Face);
			Reference refer1 = uidoc.Selection.PickObject(ObjectType.Element);
			Element elem = doc.GetElement(refer1);
			FamilyInstance inst = elem as FamilyInstance;
			Reference indexRef = null;
			List<Reference> refes = new List<Reference>();

			if (inst != null)
			{
				Autodesk.Revit.DB.Options geomOptions = doc.Application.Create.NewGeometryOptions();
				if (geomOptions != null)
				{
					geomOptions.ComputeReferences = true;
					geomOptions.IncludeNonVisibleObjects = true;
					geomOptions.View = sectionView;
				}
				GeometryElement gElement = inst.get_Geometry(geomOptions);
				GeometryInstance gInst = gElement.First() as GeometryInstance;
				String sampleStableRef = null;
				String customStableRef = null;
				if (gInst != null)
				{
					GeometryElement gSymbol = gInst.GetSymbolGeometry();
					if (gSymbol != null && gSymbol.Count() > 0)
					{
						Solid solid = null;
						foreach (var sl in gSymbol)
						{
							if (sl is Solid)
							{
								var sll = sl as Solid;
								if (sll.Faces.IsEmpty == false)
								{
									solid = sll;
								}
							}
						}
						foreach (Face face in solid.Faces)
						{

							//Face face = FindFaceByValue(solid.Faces, upConstrainValue);
							sampleStableRef = face.Reference.ConvertToStableRepresentation(doc);
							if (sampleStableRef != null)
							{
								String[] tokenList = sampleStableRef.Split(new char[] { ':' });
								customStableRef = tokenList[0] + ":" + tokenList[1] + ":" + tokenList[2] + ":" + tokenList[3] + ":" + tokenList[4];
								indexRef = Reference.ParseFromStableRepresentation(doc, customStableRef);
								GeometryObject geoObj = inst.GetGeometryObjectFromReference(indexRef);
								if (geoObj != null)
								{
									String finalToken = "";
									if (geoObj is Face)
									{
										finalToken = ":SURFACE";
									}
									customStableRef += finalToken;
									indexRef = Reference.ParseFromStableRepresentation(doc, customStableRef);
									refes.Add(indexRef);
								}

							}
						}
					}
				}
			}
			foreach (Reference reference2 in refes)
			{
				try
				{
					doc.Create.NewAlignment(uidoc.ActiveView, reference1, reference2);
				}
				catch
				{
					continue;
				}
			}

		}
	}
}

