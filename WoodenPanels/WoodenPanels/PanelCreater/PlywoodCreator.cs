﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using WoodenPanels.Utils;
using WoodenPanels.Data;

namespace WoodenPanels.PanelCreater
{
   internal static class PlywoodCreator
   {
        private  const double offset = 0.03125;
        internal static IList<Wall> CreatePlywood(
			Document doc, 
			Line panel_line, 
			ElementId levelId,
			Wall wall,
			UIData uIData,
			Application app)
        {
            IList<Wall> walls = new List<Wall>();
            //Create Plywood
            Wall plywall = Wall.Create(doc, panel_line, levelId, false);

            //Add wall type to the plywood wall
            plywall.WallType = GetWallType.GetType(doc,app,uIData.profile_type);
            
            // Get offset curve
            Curve newlocation = GetOffsetCurve(plywall, wall, offset);
            
            // Get line to split wall chain
            Line wallLine = newlocation as Line;

            //Check lenth of curve 
            if (newlocation.Length >= 1)
            {
                if (newlocation.Length <= 8)
                {
                    Wall wall1 = Wall.Create(doc, newlocation, plywall.LevelId, true);
                    wall1.WallType = GetWallType.GetType(doc,app, uIData.profile_type);
                  
                    /*WallUtils.DisallowWallJoinAtEnd(wall1, 1);
                    WallUtils.DisallowWallJoinAtEnd(wall1, 0);*/
                    wall1.SetUIValue(uIData);
                    walls.Add(wall1);
                }
                else
                {
                    XYZ start_point = newlocation.GetEndPoint(0);
                    XYZ end_point = null;
                  
                    double last_part = newlocation.Length % 8;
                    
                    if (last_part < 1)
                    {
                        for (int i = 1; i < newlocation.Length / 8; i++)
                        {


                            if (i >= Math.Floor(newlocation.Length / 8))
                            {
                                end_point = start_point + wallLine.Direction * 4;
                                Line newLine = Line.CreateBound(start_point, end_point);
                                Wall wall1 = Wall.Create(doc, newLine, plywall.LevelId, true);
                                wall1.WallType = GetWallType.GetType(doc,app, uIData.profile_type);
                                wall1.SetUIValue(uIData);
                                /*WallUtils.DisallowWallJoinAtEnd(wall1, 1);
                                WallUtils.DisallowWallJoinAtEnd(wall1, 0);*/
                                start_point = end_point;
                                end_point = start_point + wallLine.Direction * (4 + last_part);
                                newLine = Line.CreateBound(start_point, end_point);
                                Wall wall12 = Wall.Create(doc, newLine, plywall.LevelId, true);

                                wall12.WallType = GetWallType.GetType(doc,app, uIData.profile_type);
                                wall12.SetUIValue(uIData);
                               /* WallUtils.DisallowWallJoinAtEnd(wall12, 1);
                                WallUtils.DisallowWallJoinAtEnd(wall12, 0);*/
                                walls.Add(wall1);
                                walls.Add(wall12);
                            }
                            else
                            {
                                end_point = start_point + wallLine.Direction * 8;
                                Line newLine = Line.CreateBound(start_point, end_point);
                                Wall wall1 = Wall.Create(doc, newLine, plywall.LevelId, true);
                                wall1.WallType = GetWallType.GetType(doc,app, uIData.profile_type);
                                wall1.SetUIValue(uIData);
                               /* WallUtils.DisallowWallJoinAtEnd(wall1, 1);
                                WallUtils.DisallowWallJoinAtEnd(wall1, 0);*/
                                start_point = end_point;
                                walls.Add(wall1);
                            }
                        }
                    }
                    else
                    {

                        for (int i = 1; i < newlocation.Length / 8; i++)
                        {


                            if (i >= Math.Floor(newlocation.Length / 8))
                            {
                                end_point = start_point + wallLine.Direction * 8;
                                Line newLine = Line.CreateBound(start_point, end_point);
                                Wall wall1 = Wall.Create(doc, newLine, plywall.LevelId, true);
                                wall1.WallType = GetWallType.GetType(doc,app, uIData.profile_type);
                                wall1.SetUIValue(uIData);
                               /* WallUtils.DisallowWallJoinAtEnd(wall1, 1);
                                WallUtils.DisallowWallJoinAtEnd(wall1, 0);*/
                                start_point = end_point;
                                end_point = start_point + wallLine.Direction * last_part;
                                newLine = Line.CreateBound(start_point, end_point);
                                Wall wall12 = Wall.Create(doc, newLine, plywall.LevelId, true);
                                /*WallUtils.DisallowWallJoinAtEnd(wall12, 1);
                                WallUtils.DisallowWallJoinAtEnd(wall12, 0);*/
                                wall12.WallType = GetWallType.GetType(doc,app, uIData.profile_type);
                                wall12.SetUIValue(uIData);
                                walls.Add(wall1);
                                walls.Add(wall12);
                            }
                            else
                            {
                                end_point = start_point + wallLine.Direction * 8;
                                Line newLine = Line.CreateBound(start_point, end_point);
                                Wall wall1 = Wall.Create(doc, newLine, plywall.LevelId, true);
                               /* WallUtils.DisallowWallJoinAtEnd(wall1, 1);
                                WallUtils.DisallowWallJoinAtEnd(wall1, 0);*/
                                wall1.WallType = GetWallType.GetType(doc,app, uIData.profile_type);
                                wall1.SetUIValue(uIData);
                                start_point = end_point;
                                walls.Add(wall1);
                            }
                        }
                    }

                }
            }
            else
            {
                TaskDialog.Show("Error", "Length of Panel is less than 1Ft! ");
            }

            
               
           
            doc.Delete(plywall.Id);
            return walls;
        }
        
        internal static Curve GetOffsetCurve (Wall plywall, Wall wall, double offset)
        {
            Autodesk.Revit.DB.LocationCurve cv = plywall.Location as Autodesk.Revit.DB.LocationCurve;
            Curve newlocation = cv.Curve.CreateTransformed(Transform.CreateTranslation(wall.Orientation * offset));

            Autodesk.Revit.DB.LocationCurve wallCV = wall.Location as Autodesk.Revit.DB.LocationCurve;
            double width = wall.Width / 2;
            double distance = wallCV.Curve.Distance(new XYZ(newlocation.GetEndPoint(1).X, newlocation.GetEndPoint(1).Y, wallCV.Curve.GetEndPoint(1).Z));
            if (distance < width)
            {
                newlocation = cv.Curve.CreateTransformed(Transform.CreateTranslation(wall.Orientation * (-offset)));
            }
            return newlocation;
        }
   }
}
