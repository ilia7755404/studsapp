﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WoodenPanels.Data;

namespace WoodenPanels.WPF
{
    
    public partial class UserControl1 : Window, IDisposable
    {
        public bool Ok { get; set; }
        public string p_Offset { get; set; }
        public string p_Height { get; set; }
        public string st_Offset { get; set; }
        public string typeofStud { get; set; }
        public UIData data;
        public List<String> family_types;
        public UserControl1()
        {


            InitializeComponent();
            PanelOffset.Text = "0";
            Panel_Height.Text = "10";
            Stud_Offset.Text = "0.875";
           

        }
        public UserControl1(UIData uIData)
        {

            data = uIData;
            InitializeComponent();
            PanelOffset.Text = uIData.panel_Offset;
            Panel_Height.Text = uIData.panel_Height;
            Stud_Offset.Text = uIData.stud_Offset;
            CB.Text = uIData.profile_type;
            

        }
        public void Dispose()
        {
            this.Close();

        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            Ok = true;
            this.Close();
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            Ok = false;
            this.Close();
        }

        private void Panel_Offset(object sender, TextChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            p_Offset = textbox.Text;
        }
        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }
        public static bool IsValid(string str)
        {
            double i;
            return double.TryParse(str, out i) && i >= 0 && i <= 9999;
        }

     

        private void CB_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            combo.ItemsSource = family_types;
            if (data != null)
            {
                combo.SelectedItem = data.profile_type;
            }
            else
            {
                combo.SelectedIndex = 0;
            }
        }

        private void CB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedcomboItem = sender as ComboBox;
            typeofStud = selectedcomboItem.SelectedItem as string;
        }

        private void TextBox_StudOffset(object sender, TextChangedEventArgs e)
        {

            var textbox = sender as TextBox;
            st_Offset = textbox.Text;
        }
        private void TextBox_Height(object sender, TextChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            
            
            p_Height = textbox.Text;
        }
    }
}
