﻿using System;
using Autodesk.Revit.DB;

namespace WoodenPanels
{
	public static class UnitConvert
	{
		public static Double FeetToMm(double value)
		{
			return UnitUtils.Convert(value,
				DisplayUnitType.DUT_DECIMAL_FEET,
				DisplayUnitType.DUT_MILLIMETERS);
		}

		public static Double FeetToM(double value)
		{
			return UnitUtils.Convert(value,
				DisplayUnitType.DUT_DECIMAL_FEET,
				DisplayUnitType.DUT_METERS);
		}

		public static Double MmToFeet(double value)
		{
			return UnitUtils.Convert(value,
				DisplayUnitType.DUT_MILLIMETERS,
				DisplayUnitType.DUT_DECIMAL_FEET);
		}

		public static Double MToFeet(double value)
		{
			return UnitUtils.Convert(value,
				DisplayUnitType.DUT_METERS,
				DisplayUnitType.DUT_DECIMAL_FEET);
		}
		public static Double DegreeToRadian(double value)
		{
			return UnitUtils.Convert(value,
				DisplayUnitType.DUT_DECIMAL_DEGREES,
				DisplayUnitType.DUT_RADIANS);
		}
		public static Double RadianToDegree(double value)
		{
			return UnitUtils.Convert(value,
				DisplayUnitType.DUT_RADIANS,
				DisplayUnitType.DUT_DECIMAL_DEGREES);
		}
		public static Double InchToFeet(double value)
		{
			return UnitUtils.Convert(value,
				DisplayUnitType.DUT_DECIMAL_INCHES,
				DisplayUnitType.DUT_DECIMAL_FEET);
		}
	}
}

