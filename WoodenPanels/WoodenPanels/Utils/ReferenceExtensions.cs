﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;

namespace WoodenPanels.Utils
{
    internal static class ReferenceExtensions
    {
        internal static XYZ GetPoint(this Reference myRef, Document doc)
        {
            Element e = doc.GetElement(myRef.ElementId);

            GeometryObject geomObj = e.GetGeometryObjectFromReference(myRef);
            
            XYZ p = myRef.GlobalPoint;
            
            return p;
        }
        
    }
}
