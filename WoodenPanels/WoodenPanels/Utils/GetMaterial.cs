﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;

namespace WoodenPanels.Utils
{
   internal static class GetMaterial
    {
        internal static Material GetMaterialForWall (Document doc)
        {
            FilteredElementCollector collector
            = new FilteredElementCollector(doc)
            .OfClass(typeof(Material));

            IEnumerable<Material> materialsEnum
              = collector.ToElements().Cast<Material>();
            Material plywood = null;

            try
            {
                plywood = materialsEnum.Where(p => p.Name == "3/4 Plywood").First();
            }

            catch
            {
                ElementId materialId = Material.Create(doc, "3/4 Plywood");
                plywood = doc.GetElement(materialId) as Material;
                plywood.Color = new Color(255, 255, 255);
                

                
            }

            return plywood;
        }
    }
}
