﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using System.Linq;
using System.IO;
#endregion

namespace WoodenPanels.Utils
{
    public class CustomCopyHandler : IDuplicateTypeNamesHandler
    {
        public DuplicateTypeAction OnDuplicateTypeNamesFound(
          DuplicateTypeNamesHandlerArgs args)
        {
            return DuplicateTypeAction.UseDestinationTypes;
        }
    }
    public class FailHandler : IFailuresPreprocessor
    {

        public FailureProcessingResult PreprocessFailures(
          FailuresAccessor a)
        {
            IList<FailureMessageAccessor> failures
              = a.GetFailureMessages();
            foreach (FailureMessageAccessor f in failures)
            {
                var txt = f.GetDescriptionText();
                var fType = f.GetFailureDefinitionId();
                if (txt == "Can't make stairs")
                {
                    a.DeleteElements(f.GetFailingElementIds().ToList());
                    return FailureProcessingResult.ProceedWithCommit;
                    //Hack
                }

                FailureSeverity fseverity = a.GetSeverity();
                if (fseverity == FailureSeverity.Warning)
                {
                    a.DeleteWarning(f);
                }
                else
                {
                    a.ResolveFailure(f);
                    return FailureProcessingResult.ProceedWithCommit;
                }

            }
            return FailureProcessingResult.Continue;
        }
    }
    internal static class GetWallType
    {
        private static string FamilyName = "Walls";
        static string _family_folder = Path.GetDirectoryName(typeof(FindFamily)
        .Assembly.Location);
        const string _family_ext = "rvt";
        static string _family_path = null;
        static string FamilyPath
        {
            get
            {
                if (null == _family_path)
                {
                    _family_path = Path.Combine(
                      _family_folder, FamilyName);

                    _family_path = Path.ChangeExtension(
                      _family_path, _family_ext);
                }
                return _family_path;
            }
        }

        internal static WallType GetType (Document doc, Application app,string walltypename)
        {
            //Thiknes of wall
            

            // Find Walltypes
            FilteredElementCollector collector = new FilteredElementCollector(doc);            
            IEnumerable<Element> wall_types = collector.OfCategory(BuiltInCategory.OST_Walls)
            .WhereElementIsElementType();

            if(wall_types.Count()!=0)
            {
                WallType plywood = null;
                try
                {
                    plywood = wall_types.Where(p => p.Name == walltypename).First() as WallType;
                }
                
                catch
                {
                    CopyWallType( doc,  app,walltypename);
                    plywood = wall_types.Where(p => p.Name ==  walltypename).First() as WallType;

                }
               
                return plywood;                
            }
            return null;
        }
        internal static void CopyWallType(Document doc,Application app,string walltypename)
        {
           
            Document templateDocument = app.OpenDocumentFile(FamilyPath);

            using (Transaction tr = new Transaction(doc))
            {
                tr.Start("copy wall");


                try
                {

                    var wallType = GetElementType(templateDocument, "Curtain wall", walltypename);
                    CopyPasteOptions options = new CopyPasteOptions();
                    options.SetDuplicateTypeNamesHandler(new CustomCopyHandler());
                    ElementTransformUtils.CopyElements(templateDocument, new List<ElementId> { wallType.Id }, doc, null, options);
                }
                catch { }

                FailureHandlingOptions failOpt = tr.GetFailureHandlingOptions();
                failOpt.SetFailuresPreprocessor(new FailHandler());
                tr.SetFailureHandlingOptions(failOpt);
                doc.Regenerate();
                tr.Commit();
            }

        }
        public static ElementType GetElementType(Document doc,
        string familyName,
        string symbolName)
        {
            var collector = new FilteredElementCollector(doc);
            FilterRule rule = new FilterStringRule(new ParameterValueProvider(new ElementId(BuiltInParameter.ALL_MODEL_FAMILY_NAME)),
                new FilterStringEquals(), familyName, false);

            ElementParameterFilter elementParameterFilter = new ElementParameterFilter(rule);

            var elementType =
                collector
                    .OfClass(typeof(ElementType))
                    .WherePasses(elementParameterFilter)
                    .OfType<ElementType>()
                    .FirstOrDefault(x => x.Name.Equals(symbolName));

            return elementType;
        }
    }
}
