﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using WoodenPanels.Utils;

namespace WoodenPanels.Utils
{
   internal static  class FaceExtensions 
   {
        internal static Face GetFaceFromPoint(Wall wall, UV uV, Document doc)
        {
            Face face = null;
            IList<Reference> faces = HostObjectUtils.GetSideFaces(wall, ShellLayerType.Exterior);
            IList<Reference> faces1 = HostObjectUtils.GetSideFaces(wall, ShellLayerType.Interior);
            foreach (Reference newface in faces)
            {
                GeometryObject geoObject = doc.GetElement(newface).GetGeometryObjectFromReference(newface);
                face = geoObject as Face;
                if (face.IsInside(uV))
                {
                    break;
                }
            }
            if (face == null)
            {
                foreach (Reference newface in faces1)
                {
                    GeometryObject geoObject = doc.GetElement(newface).GetGeometryObjectFromReference(newface);
                    face = geoObject as Face;
                    if (face.IsInside(uV))
                    {
                        break;
                    }
                }
            }
            return face;
        }


    }
}
