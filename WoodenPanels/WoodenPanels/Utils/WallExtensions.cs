﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using WoodenPanels.Utils;
using WoodenPanels.Data;

namespace WoodenPanels.Utils
{
    internal static class WallExtensions
    {
        //Find intersection
        internal static void FlipIntersectedWall(this Wall wall, IList<Wall> walls, Document doc)
        {
            var curtainGrids = walls.Select(p => p.CurtainGrid);
            foreach (CurtainGrid curtain in curtainGrids)
            {
                Mullion mullion = doc.GetElement(curtain.GetMullionIds().First()) as Mullion;

                XYZ xYZ = null;
                LocationCurve curve = wall.Location as LocationCurve;
                Curve curve1 = curve.Curve;
                Autodesk.Revit.DB.Options options = new Autodesk.Revit.DB.Options();

                GeometryElement geometryElement = mullion.get_Geometry(options);
                foreach (GeometryObject geometryObject in geometryElement)
                {
                    GeometryInstance instance = geometryObject as GeometryInstance;
                    if (instance != null)
                    {
                        foreach (GeometryObject geometry in instance.GetInstanceGeometry())
                        {
                            Solid solid = geometry as Solid;
                            if (solid != null)
                            {
                                xYZ = solid.ComputeCentroid();
                                break;
                            }
                        }
                    }
                }
                Line line = Line.CreateBound(new XYZ(curve1.GetEndPoint(0).X, curve1.GetEndPoint(0).Y, xYZ.Z),
                    new XYZ(curve1.GetEndPoint(1).X, curve1.GetEndPoint(1).Y, xYZ.Z));
                double distance = line.Distance(xYZ);
                if (Math.Round(distance, 3) <= Math.Round(wall.Width / 2, 3))
                {
                    using (Transaction tx = new Transaction(doc))
                    {
                        tx.Start("Flipping Wall");
                        foreach (Wall wall1 in walls)
                        {
                            wall1.Flip();
                        }
                        doc.Regenerate();
                        tx.Commit();
                    }
                    break;
                }
                else
                {
                    break;
                }
            }
        }


        // get faces for family
        static private Solid ExtractFirstSolidByFamilyInstance(Element instance)
        {

            var options = new Autodesk.Revit.DB.Options();
            options.ComputeReferences = true;
            options.DetailLevel = ViewDetailLevel.Undefined;
            options.IncludeNonVisibleObjects = true;

            var geomElem = instance.get_Geometry(options);

           
            foreach(GeometryObject geomObj in geomElem)
            {
                Autodesk.Revit.DB.GeometryInstance geomInst = geomObj as Autodesk.Revit.DB.GeometryInstance;
                if(geomInst!=null)
                {
                    Autodesk.Revit.DB.GeometryElement transformedGeomElem
                        = geomInst.GetSymbolGeometry();
                   foreach(GeometryObject obj in transformedGeomElem)
                    {
                        Solid solid = obj as Solid;
                        if(solid!=null)
                        {
                            
                            return solid;
                        }
                    }
                }
            }
            return null;
        }


        internal static  IList<XYZ> GetTransformPoints(this Wall wall,Face face,Document doc)
        {
            CurtainGrid curtainGrid = wall.CurtainGrid;
            IList<Element> panels = new List<Element>();
            foreach (ElementId id in curtainGrid.GetPanelIds())
            {
                Element elem = doc.GetElement(id);
                panels.Add(elem);
            }
            Element instance = panels.OrderBy(p => ExtractFirstSolidByFamilyInstance(p).ComputeCentroid().Z).Select(p => p).First();
            IList < XYZ > points= new List<XYZ>();
            var options = new Autodesk.Revit.DB.Options();
            options.ComputeReferences = true;
            options.DetailLevel = ViewDetailLevel.Undefined;
            options.IncludeNonVisibleObjects = true;

            var geomElem = instance.get_Geometry(options);
            foreach (GeometryObject geomObj in geomElem)
            {
                Autodesk.Revit.DB.GeometryInstance geomInst = geomObj as Autodesk.Revit.DB.GeometryInstance;
                if (geomInst != null)
                {
                    Autodesk.Revit.DB.GeometryElement transformedGeomElem
                        = geomInst.GetSymbolGeometry();
                    foreach (GeometryObject obj in transformedGeomElem)
                    {
                        Solid solid = obj as Solid;
                        Transform transform = geomInst.Transform;
                        if (solid != null)
                        {
                           foreach(Face face1 in solid.Faces)
                            {
                                if(face1.Reference.ElementId==face.Reference.ElementId)
                                {
                                    Mesh mesh = face.Triangulate();
                                    foreach (XYZ ii in mesh.Vertices)
                                    {
                                        XYZ point = ii;
                                        XYZ transformedPoint =transform.OfPoint(point);
                                        points.Add(transformedPoint);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return points;
        }

        internal static bool GetFAceNormal(this Wall wall,Face face, Document doc)
        {
            CurtainGrid curtainGrid = wall.CurtainGrid;
            IList<Element> panels = new List<Element>();
            foreach (ElementId id in curtainGrid.GetPanelIds())
            {
                Element elem = doc.GetElement(id);
                panels.Add(elem);
            }
            Element instance = panels.OrderBy(p => ExtractFirstSolidByFamilyInstance(p).ComputeCentroid().Z).Select(p => p).First();

            var options = new Autodesk.Revit.DB.Options();
            options.ComputeReferences = true;
            options.DetailLevel = ViewDetailLevel.Undefined;
            options.IncludeNonVisibleObjects = true;

            var geomElem = instance.get_Geometry(options);
            foreach (GeometryObject geomObj in geomElem)
            {
                Autodesk.Revit.DB.GeometryInstance geomInst = geomObj as Autodesk.Revit.DB.GeometryInstance;
                if (geomInst != null)
                {
                    Transform transform = geomInst.Transform;
                    Autodesk.Revit.DB.GeometryElement transformedGeomElem
                        = geomInst.GetSymbolGeometry();
                 
                    foreach (GeometryObject obj in transformedGeomElem)
                    {
                        Solid solid = obj as Solid;
                       
                        if (solid != null)
                        {

                            foreach (Face face1 in solid.Faces)
                            {
                                
                                PlanarFace planar = face1 as PlanarFace;
                                XYZ normalFace = planar.FaceNormal;
                                XYZ transformvector = transform.OfVector(normalFace);
                                if(wall.Orientation.IsAlmostEqualTo(transformvector))
                                {
                                    if (face.Area == face1.Area)
                                    {


                                        if (face1.GetHashCode() == face.GetHashCode())
                                        { return true; }
                                    }
                                }
                               
                                                        
                            }
                        }
                    }
                }
            }
            return false; 
        }


        internal static double  GetFacetoMull(this Wall wall, Face face,XYZ xYZ, Document doc)
        {
            CurtainGrid curtainGrid = wall.CurtainGrid;
            IList<Element> panels = new List<Element>();
            foreach (ElementId id in curtainGrid.GetPanelIds())
            {
                Element elem = doc.GetElement(id);
                panels.Add(elem);
            }
            Element instance = panels.OrderBy(p => ExtractFirstSolidByFamilyInstance(p).ComputeCentroid().Z).Select(p => p).First();
            
            var options = new Autodesk.Revit.DB.Options();
            options.ComputeReferences = true;
            options.DetailLevel = ViewDetailLevel.Undefined;
            options.IncludeNonVisibleObjects = true;

            var geomElem = instance.get_Geometry(options);
            foreach (GeometryObject geomObj in geomElem)
            {
                Autodesk.Revit.DB.GeometryInstance geomInst = geomObj as Autodesk.Revit.DB.GeometryInstance;
                if (geomInst != null)
                {
                    Autodesk.Revit.DB.GeometryElement transformedGeomElem
                        = geomInst.GetSymbolGeometry();
                    foreach (GeometryObject obj in transformedGeomElem)
                    {
                        Solid solid = obj as Solid;
                        Transform transform = geomInst.Transform;
                        if (solid != null)
                        {
                           
                            foreach (Face face1 in solid.Faces)
                            {
                                if (face1.Reference.ElementId == face.Reference.ElementId)
                                {
                                    Curve curve = GetLowestCurve(face1);
                                   
                                        XYZ point = curve.GetEndPoint(0);
                                        XYZ transformedPoint = transform.OfPoint(point);
                                    XYZ point1 = curve.GetEndPoint(1);
                                    XYZ transformedPoint1 = transform.OfPoint(point1);
                                    Line line = Line.CreateBound(transformedPoint, transformedPoint1);
                                     return line.Distance(xYZ);
                                        
                                    
                                    
                                }
                            }
                        }
                    }
                }
            }
            return 0;
        }




        internal static Face GetPanelFaceForFamily1(this Wall wall, Document doc)
        {
            CurtainGrid curtainGrid = wall.CurtainGrid;
            IList<Element> panels = new List<Element>();
            foreach (ElementId id in curtainGrid.GetPanelIds())
            {
                Element elem = doc.GetElement(id);
                panels.Add(elem);
            }

            Element element = panels.OrderBy(p => ExtractFirstSolidByFamilyInstance(p).ComputeCentroid().Z).Select(p => p).First();


            var options = new Autodesk.Revit.DB.Options();
            options.ComputeReferences = true;
            options.DetailLevel = ViewDetailLevel.Undefined;
            options.IncludeNonVisibleObjects = true;

            var geomElem = element.get_Geometry(options);

           
            foreach (GeometryObject geomObj in geomElem)
            {
                Autodesk.Revit.DB.GeometryInstance geomInst = geomObj as Autodesk.Revit.DB.GeometryInstance;
                if (geomInst != null)
                {
                    Transform transform = geomInst.Transform;
                    
                    Autodesk.Revit.DB.GeometryElement transformedGeomElem
                        = geomInst.GetSymbolGeometry(transform);
                    foreach (GeometryObject obj in transformedGeomElem)
                    {
                        Solid solid = obj as Solid;
                        if (solid != null)
                        {
                            foreach(Face face in solid.Faces)
                            {
                                if((face as PlanarFace).FaceNormal==wall.Orientation)
                                {
                                    return face;
                                }
                            }
                           
                        }
                    }
                }
            }



            return null;

        }



        internal static Face GetPanelFaceForFamily(this Wall wall, Document doc)
        {
            CurtainGrid curtainGrid = wall.CurtainGrid;
            IList<Element> panels = new List<Element>();
            foreach (ElementId id in curtainGrid.GetPanelIds())
            {
                Element elem = doc.GetElement(id);
                panels.Add(elem);
            }

            Solid element = panels.OrderBy(p => ExtractFirstSolidByFamilyInstance(p).ComputeCentroid().Z).Select(p => ExtractFirstSolidByFamilyInstance(p)).First();
            Element mull = doc.GetElement(curtainGrid.GetMullionIds().First());
            List<Face> faces = new List<Face>();
            foreach (Face face in element.Faces)
            {
                faces.Add(face);
                

            }
            
            var area = faces.Where(p => p.Area == faces.OrderBy(i => i.Area).Select(i => i.Area).Last()).Select(p => p.Area).First();
            //Face face1 = faces.Where(p => p.Area == area).OrderBy(p => wall.GetFacetoMull(p,(mull.Location as LocationPoint).Point,doc)).Select(p => p).First();
            Face face1 = faces.Where(p => wall.GetFAceNormal(p, doc )==true).Select(p => p).First();
            return face1;

        }
        internal static Curve GetLowestCurve (Face face)
        {
            List<Curve> curves = new List<Curve>();
            foreach(Curve curve in face.GetEdgesAsCurveLoops().First())
            {
                curves.Add(curve);
            }
            Curve curve1 = curves.Where(p => p.GetEndPoint(0).Z == p.GetEndPoint(0).Z).OrderBy(p => p.GetEndPoint(0).Z).Select(p => p).First();
            return curve1;
        }
        
        internal static Reference GetFaceForFamily(this Wall wall, Document doc)
        {
            CurtainGrid curtainGrid = wall.CurtainGrid;
            IList<Wall> walls = new List<Wall>();
            // Secondly, find corresponding panel wall
            // for the curtain wall and retrieve the actual
            // geometry from that.
            FilteredElementCollector cwPanels
              = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_CurtainWallPanels)
                .OfClass(typeof(Wall));

            foreach (Wall cwp in cwPanels)
            {
                // Find panel wall belonging to this curtain wall
                // and retrieve its geometry

                if (cwp.StackedWallOwnerId == wall.Id)
                {
                    walls.Add(cwp);
                }
            }
            Wall wall1 = walls.OrderBy(p => p.GetBiggestFace(doc)).Select(p => p).Last();
            return HostObjectUtils.GetSideFaces(wall1,ShellLayerType.Exterior).First();
        }
        internal static double GetBiggestFace(this Wall wall, Document doc)
        {
            List<Face> faces = new List<Face>();
            Autodesk.Revit.DB.Options options = new Autodesk.Revit.DB.Options();

            options.ComputeReferences = true;
            options.IncludeNonVisibleObjects = true;
            GeometryElement geometry = wall.get_Geometry(options);
            foreach (GeometryObject geometry1 in geometry)
            {
                Solid solid = geometry1 as Solid;
                if (solid != null)
                {
                    foreach (Face face1 in solid.Faces)
                    {
                        faces.Add(face1);
                    }
                }
            }
            var references = faces.Where(p => p.Area == faces.OrderBy(i => i.Area).Select(i => i.Area).Last()).Select(p => p.Area).First();
            return references;
        }

        internal static Face GetTopFace(this Wall wall,  Document doc)
        {
            List<Face> faces = new List<Face>();
            Autodesk.Revit.DB.Options opt = new Autodesk.Revit.DB.Options();
            Autodesk.Revit.DB.GeometryElement geomElem = wall.get_Geometry(opt);
            foreach (GeometryObject geomObj in geomElem)
            {
                Solid geomSolid = geomObj as Solid;
                if (null != geomSolid)
                {
                    foreach (Face geomFace in geomSolid.Faces)
                    {
                        faces.Add(geomFace);
                    }
                    break;
                }
            }
                return  faces.OrderBy(p => p.Reference.GlobalPoint.Z).Select(p=>p).Last();
        }
        internal static Reference GetBottomFace(this Wall wall, Document doc)
        {
            return HostObjectUtils.GetBottomFaces(wall).First();
        }
       
        private static Face GetFace (Reference newface,Document doc)
        {
            GeometryObject geoObject = doc.GetElement(newface).GetGeometryObjectFromReference(newface);
            Face face = geoObject as Face;

            return face;
        }
      
        internal static Parameter GetHeight (this Wall wall )
        {
            
            Parameter parameter = wall.get_Parameter(BuiltInParameter.WALL_USER_HEIGHT_PARAM);
            return parameter;
        }
        internal static Parameter GetOffset(this Wall wall)
        {
            
            Parameter parameter = wall.get_Parameter(BuiltInParameter.WALL_BASE_OFFSET);
            return parameter;
        }
        internal static void SetUIValue(this Wall wall,UIData uIData)
        {
			Parameter parameterHeight = wall.get_Parameter(BuiltInParameter.WALL_USER_HEIGHT_PARAM);
			var wallH = Convert.ToDouble(uIData.panel_Height);
			parameterHeight.Set(wallH);
            Parameter parameterBaseOffset = wall.GetOffset();
            parameterBaseOffset.Set(Convert.ToDouble(uIData.panel_Offset));
        }
        internal static  void GetWallsToSetStr(this Wall wall, Document doc)
        {
            CurtainGrid curtainGrid = wall.CurtainGrid;
            IList<Wall> walls = new List<Wall>();
            // Secondly, find corresponding panel wall
            // for the curtain wall and retrieve the actual
            // geometry from that.
            FilteredElementCollector cwPanels
              = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_CurtainWallPanels)
                .OfClass(typeof(Wall));

            foreach (Wall cwp in cwPanels)
            {
                // Find panel wall belonging to this curtain wall
                // and retrieve its geometry

                if (cwp.StackedWallOwnerId == wall.Id)
                {
                    walls.Add(cwp);
                }
            }
            foreach(Wall wall1 in walls)
            {
                Parameter parameterHeight = wall1.get_Parameter(BuiltInParameter.WALL_STRUCTURAL_SIGNIFICANT);
                parameterHeight.Set(1);
            }
            
        }
    }
}
