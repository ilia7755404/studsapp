﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using WoodenPanels.Utils;
using System.Reflection;
using System.IO;

namespace WoodenPanels.Utils
{
    internal static class FindFamily
    {
        private static string FamilyName = "Stud";
        static string _family_folder = Path.GetDirectoryName(typeof(FindFamily)
        .Assembly.Location);
        const string _family_ext = "rfa";
        static string _family_path = null;
        static string FamilyPath
        {
            get
            {
                if (null == _family_path)
                {
                    _family_path = Path.Combine(
                      _family_folder, FamilyName);

                    _family_path = Path.ChangeExtension(
                      _family_path, _family_ext);
                }
                return _family_path;
            }
        }
        private static Element FindElementByName(Document doc)
        {
            return new FilteredElementCollector(doc)
              .OfClass(typeof(Family))
              .FirstOrDefault<Element>(
                e => e.Name.Equals(FamilyName));


        }
        internal static Family GetFamily(Document doc)
        {
            Family family = FindElementByName(doc) as Family;

            if (null == FindElementByName(doc))
            {
                // It is not present, so check for 
                // the file to load it from:



                // Load family from file:

                using (Transaction tx = new Transaction(doc))
                {
                    tx.Start("Load Family");
                    doc.LoadFamily(FamilyPath, out family);
                    tx.Commit();
                }
                family = FindElementByName(doc) as Family;
            }
            return family;

        }
        internal static IEnumerable<String> GetFamilyTypes (Document doc)
        {
            Family family = GetFamily(doc);
            IEnumerable<string> symbolsName = family.GetFamilySymbolIds().Select(p =>  doc.GetElement(p).Name);
            return symbolsName;

        }
    }
    
}
