﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoodenPanels.Data
{
    public class UIData
    {
        public string panel_Offset { get; set; }
        public string panel_Height { get; set; }

        public string profile_type { get; set; }

        public string stud_Offset { get; set; }
    }
}
