﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using WoodenPanels.Utils;
using WoodenPanels.PanelCreater;
using WoodenPanels.Data;


namespace WoodenPanels
{
   internal static class PanelCreator
   {
        internal static void CreatePanel (Document doc, UIDocument uidoc, UIData uiData,Application app)
        {
			//Get reference



			try
			{
				Reference ref_One = uidoc.Selection.PickObject(ObjectType.PointOnElement);
				Reference ref_Two = uidoc.Selection.PickObject(ObjectType.PointOnElement);





				// Get level id
				ElementId levelId = uidoc.ActiveView.GenLevel.Id;
				Element element = doc.GetElement(ref_One);

				//Get Wall Points
				XYZ point_one = ref_One.GetPoint(doc);
				XYZ point_two = ref_Two.GetPoint(doc);

				//UV point to Find Face for sweep
				UV uv = ref_One.UVPoint;

				//Get Target Wall
				Element e = doc.GetElement(ref_One);
				Wall wall = e as Wall;

				//Get face of target wall
				Face face = FaceExtensions.GetFaceFromPoint(wall, ref_One.UVPoint, doc);

				//Create Location Line for wall
				Line panel_line = Line.CreateBound(point_one, point_two);
				//Start Creation Panel
				IList<Wall> walls = new List<Wall>();
				GetWallType.CopyWallType(doc, app, uiData.profile_type);

				using (Transaction tx = new Transaction(doc))
				{
					tx.Start("Creatin wall");
					//Create plywood Panels
					walls = PlywoodCreator.CreatePlywood(doc, panel_line, levelId, wall, uiData, app);
					doc.Regenerate();
					tx.Commit();
				}
				wall.FlipIntersectedWall(walls, doc);
				/*foreach( Wall wall1 in walls)
				{
					wall1.GetWallsToSetStr(doc);
				}*/

				var viewTypeElevation = new FilteredElementCollector(doc)
				.OfClass(typeof(ViewFamilyType))
				.OfType<ViewFamilyType>()
				.FirstOrDefault(v => v.ViewFamily == ViewFamily.Section);


				var bb = wall.get_BoundingBox(uidoc.ActiveView);

				View sectView = null;

				using (Transaction tx = new Transaction(doc))
				{
					tx.Start("section view creating");
					sectView = CreateSectionView(wall, viewTypeElevation, doc);
					tx.Commit();
				}

				StudCreator.CreateStud(doc, walls, uiData.profile_type, uiData.stud_Offset, sectView, uidoc);
			}
			catch { }
		}

		static View CreateSectionView(Wall wall, ViewFamilyType viewTypeElevation, Document doc)
		{
			var locCurve = wall.Location as LocationCurve;
			var line = locCurve.Curve;

			XYZ p = line.GetEndPoint(0);
			XYZ q = line.GetEndPoint(1);
			XYZ v = q - p;

			BoundingBoxXYZ bb = wall.get_BoundingBox(null);
			double minZ = bb.Min.Z;
			double maxZ = bb.Max.Z;

			double w = v.GetLength();
			double h = maxZ - minZ;
			double d = wall.WallType.Width;
			double offset = 0.1 * w;

			XYZ min = new XYZ(-w, minZ - offset, -offset);
			XYZ max = new XYZ(w, maxZ + offset, 0);

			XYZ midpoint = p + 0.5 * v;
			XYZ walldir = v.Normalize();
			XYZ up = XYZ.BasisZ;
			XYZ viewdir = walldir.CrossProduct(up);

			Transform t = Transform.Identity;
			t.Origin = midpoint;
			t.BasisX = walldir;
			t.BasisY = up;
			t.BasisZ = viewdir;

			BoundingBoxXYZ sectionBox = new BoundingBoxXYZ();
			sectionBox.Transform = t;
			sectionBox.Min = min;
			sectionBox.Max = max;

			try
			{
				return ViewSection.CreateSection(doc, viewTypeElevation.Id, sectionBox);
			}
			catch
			{
			}
			return null;
		}
	}
}
