﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using WoodenPanels.Utils;
using WoodenPanels.PanelCreater;
using Newtonsoft.Json;
using WoodenPanels.WPF;
using WoodenPanels.Data;
using System.IO;
using Newtonsoft.Json.Converters;

namespace WoodenPanels
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class Options : IExternalCommand
    {
        static string _family_folder = Path.GetDirectoryName(typeof(FindFamily)
        .Assembly.Location);
        const string _family_ext = "json";
        static string _family_path = null;
        static string FamilyPath
        {
            get
            {
                if (null == _family_path)
                {
                    _family_path = Path.Combine(
                      _family_folder, "options");

                    _family_path = Path.ChangeExtension(
                      _family_path, _family_ext);
                }
                return _family_path;
            }
        }
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication revitApp = commandData.Application;
            UIDocument uidoc = revitApp.ActiveUIDocument;
            Document doc = uidoc.Document;

            IEnumerable <string> family_types = FindFamily.GetFamilyTypes(doc);

            UIData uiData = null;
            try
            {
                uiData = JsonConvert.DeserializeObject<UIData>(File.ReadAllText(FamilyPath));
                using (UserControl1 ui = new UserControl1(uiData))
                {
                    ui.family_types = family_types.ToList();
                    ui.ShowDialog();
                    if (!ui.Ok)
                    {
                        return Result.Cancelled;
                    }
                    else
                    {
                        UIData newData = new UIData();
                        newData.panel_Height = ui.p_Height;
                        newData.panel_Offset = ui.p_Offset;
                        newData.profile_type = ui.typeofStud;
                        newData.stud_Offset = ui.st_Offset;



                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Converters.Add(new JavaScriptDateTimeConverter());
                        serializer.NullValueHandling = NullValueHandling.Ignore;

                        using (StreamWriter sw = new StreamWriter(FamilyPath))
                        using (JsonWriter writer = new JsonTextWriter(sw))
                        {
                            serializer.Serialize(writer, newData);
                            // {"ExpiryDate":new Date(1230375600000),"Price":0}
                        }
                    }

                }
            }
            catch {
                using (UserControl1 ui = new UserControl1())
                {
                    ui.family_types = family_types.ToList();
                    ui.ShowDialog();
                    if (!ui.Ok)
                    {
                        return Result.Cancelled;
                    }
                    else
                    {
                        UIData newData = new UIData();
                        newData.panel_Height = ui.p_Height;
                        newData.panel_Offset = ui.p_Offset;
                        newData.profile_type = ui.typeofStud;
                        newData.stud_Offset = ui.st_Offset;



                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Converters.Add(new JavaScriptDateTimeConverter());
                        serializer.NullValueHandling = NullValueHandling.Ignore;

                        using (StreamWriter sw = new StreamWriter(FamilyPath))
                        using (JsonWriter writer = new JsonTextWriter(sw))
                        {
                            serializer.Serialize(writer, newData);
                            // {"ExpiryDate":new Date(1230375600000),"Price":0}
                        }
                    }

                }
            }

          

                return Result.Succeeded;
        }
    }
}
