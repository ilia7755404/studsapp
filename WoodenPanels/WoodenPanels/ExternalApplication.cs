﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using System.IO;

namespace WoodenPanels
{
    class ExternalApplication : IExternalApplication
    {
        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }
		public Result OnStartup(UIControlledApplication application)
		{

			string tabName = "Wood";
			//создаем новое панель 



			string path = Assembly.GetExecutingAssembly().Location;
			PushButtonData button = new PushButtonData("butt1", "wood", path, "WoodenPanels.PanelExeuter");

			PushButtonData button1 = new PushButtonData("butt2", "options", path, "WoodenPanels.Options");


			try { application.CreateRibbonTab(tabName); }
			catch { }
			Autodesk.Revit.UI.RibbonPanel panel = application.CreateRibbonPanel(tabName, "Automatation");

			PushButton push = panel.AddItem(button) as PushButton;
			PushButton push1 = panel.AddItem(button1) as PushButton;

			//push.LargeImage = PngImageSource("OpenningManager.Resources.fff_h2kBuOT4.png");

			return Result.Succeeded;
		}
		/*private System.Windows.Media.ImageSource PngImageSource(string embeddedPath)
		{
			Stream stream = this.GetType().Assembly.GetManifestResourceStream(embeddedPath);
			var decoder = new System.Windows.Media.Imaging.PngBitmapDecoder(stream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);

			return decoder.Frames[0];
		}*/
	}
}
